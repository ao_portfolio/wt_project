
import mkcert from 'vite-plugin-mkcert'


export default {
  build: {
    sourcemap: true,
  },
  server: {
    host: false,
    https: true,
  },
  plugins: [ mkcert() ]
}
