import * as THREE from "three";
import { GLTFLoader } from "three/addons/loaders/GLTFLoader.js";
// import { FBXLoader } from "three/addons/loaders/FBXLoader.js";
import { OrbitControls } from "three/addons/controls/OrbitControls.js";
import { VRButton } from "three/addons/webxr/VRButton.js";
import {
  CSS2DRenderer,
  CSS2DObject,
} from "three/addons/renderers/CSS2DRenderer";

import { arrowMarker, map } from "./minimap";
import { Point } from "ol/geom";
import { fromLonLat, transform } from "ol/proj";
import { Clock } from "three";
import { CircleGeometry } from "three";
import { Vector3 } from "three";

//#region settings
const buttonPressTime = 1;
const userStartPosition = new Vector3(0, 0, 0);
const cameraStartPosition = new Vector3(0, 100, 5);
//#endregion

//#region setup basics
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);

camera.position.copy(cameraStartPosition);

//renderer
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.getElementById("canvas").appendChild(renderer.domElement);

//set light
function setLights() {
  const lights = [];
  lights[0] = new THREE.PointLight(0xfffffff, 0.5, 0);

  lights[0].position.set(0, 200, 0);

  scene.add(lights[0]);
  scene.add(new THREE.AmbientLight(0xaaaaaa, 1));
}
setLights();
//#endregion

//#region Import model
const loaderCampus = new GLTFLoader(); //new GLTFLoader();
loaderCampus.load("assets/campus.glb", (gltf) => {
  var campus = gltf.scene;
  campus.position.set(-120, 0, 50);
  scene.add(campus);
});
//#endregion

//#region OrbitalControls
var controls = new OrbitControls(camera, renderer.domElement);

controls.keys = {
  UP: "ArrowUp",
  BOTTOM: "ArrowDown",
  LEFT: "ArrowLeft", //left arrow
  RIGHT: "ArrowRight", // right arrow
};
controls.listenToKeyEvents(window);
controls.keyPanSpeed = 15;
//#endregion

//#region enable webVR
var VRbutton = VRButton.createButton(renderer);
document.body.appendChild(VRbutton);
renderer.xr.enabled = true;
//#endregion

//#region user object
let walkImage = new THREE.TextureLoader().load("assets/walking.png");
let standStillImage = new THREE.TextureLoader().load("assets/standStill.png");
let crosshairImage = new THREE.TextureLoader().load("assets/crosshair.png");

const buttonGeometry = new THREE.CircleGeometry(0.5, 32);
const buttonMaterial = new THREE.MeshBasicMaterial({
  map: walkImage,
  color: 0xffffff,
});
const button = new THREE.Mesh(buttonGeometry, buttonMaterial);
button.position.y = 0.02;
button.rotation.x = -Math.PI / 2;

const buttonIndicatorGeometry = new THREE.CircleGeometry(
  0.55,
  32,
  0,
  Math.PI * 2
);
const buttonIndicatorMaterial = new THREE.MeshBasicMaterial({
  color: 0x33c2ff,
});
const buttonIndicator = new THREE.Mesh(
  buttonIndicatorGeometry,
  buttonIndicatorMaterial
);
buttonIndicator.position.y = 0.01;
buttonIndicator.rotation.x = -Math.PI / 2;

const crosshairMaterial = new THREE.SpriteMaterial({ map: crosshairImage });
const crosshair = new THREE.Sprite(crosshairMaterial);
crosshair.scale.set(0.1, 0.1, 0.1);

var user = new THREE.Group();
user.add(camera);
user.add(button);
user.add(buttonIndicator);
user.add(crosshair);
user.position.copy(userStartPosition);
scene.add(user);
//#endregion

//#region infopanels
//infopanel
const infopanelsGroup = new THREE.Group();
scene.add(infopanelsGroup);

function createInfoPanel(position, imagePath) {
  const image = new THREE.TextureLoader().load(imagePath);
  const spriteMaterial = new THREE.SpriteMaterial({ map: image });
  const sprite = new THREE.Sprite(spriteMaterial);
  sprite.scale.set(20, 10, 20);
  sprite.position.copy(position);
  infopanelsGroup.add(sprite);
  sprite.visible = false;
}

//infoplaceholder
const infoplaceholdersGroup = new THREE.Group();
scene.add(infoplaceholdersGroup);

const infoImage = new THREE.TextureLoader().load("../assets/info.png");
const emptyImage = new THREE.TextureLoader().load("../assets/empty.png");
function createInfoPlaceholder(position) {
  const spriteMaterial = new THREE.SpriteMaterial({ map: infoImage });
  const sprite = new THREE.Sprite(spriteMaterial);
  sprite.scale.set(5, 5, 5);
  sprite.position.copy(position);
  infoplaceholdersGroup.add(sprite);
}

//construction panels & placeholders
const locationA = new THREE.Vector3(-10, 10, -60);
createInfoPanel(locationA, "../assets/A-Blok.png");
createInfoPlaceholder(locationA);

const locationB = new THREE.Vector3(100, 10, -30);
createInfoPanel(locationB, "../assets/B-Blok.png");
createInfoPlaceholder(locationB);

const locationC = new THREE.Vector3(80, 5, -5);
createInfoPanel(locationC, "../assets/C-Blok.png");
createInfoPlaceholder(locationC);

const locationD = new THREE.Vector3(60, 10, 20);
createInfoPanel(locationD, "../assets/D-Blok.png");
createInfoPlaceholder(locationD);

const locationE = new THREE.Vector3(10, 10, 0);
createInfoPanel(locationE, "../assets/E-Blok.png");
createInfoPlaceholder(locationE);

const locationF = new THREE.Vector3(-80, 10, -80);
createInfoPanel(locationF, "../assets/F-Blok.png");
createInfoPlaceholder(locationF);

const locationG = new THREE.Vector3(-20, 10, -110);
createInfoPanel(locationG, "../assets/G-Blok.png");
createInfoPlaceholder(locationG);

const locationH = new THREE.Vector3(50, 8, -130);
createInfoPanel(locationH, "../assets/H-Blok.png");
createInfoPlaceholder(locationH);

const locationI = new THREE.Vector3(70, 8, -100);
createInfoPanel(locationI, "../assets/I-Blok.png");
createInfoPlaceholder(locationI);

const locationJ = new THREE.Vector3(110, 8, -110);
createInfoPanel(locationJ, "../assets/J-Blok.png");
createInfoPlaceholder(locationJ);

const locationL = new THREE.Vector3(-40, 20, 0);
createInfoPanel(locationL, "../assets/L-Blok.png");
createInfoPlaceholder(locationL);

const locationM = new THREE.Vector3(-70, 20, 0);
createInfoPanel(locationM, "../assets/M-Blok.png");
createInfoPlaceholder(locationM);

const locationP = new THREE.Vector3(-40, 10, -85);
createInfoPanel(locationP, "../assets/P-Blok.png");
createInfoPlaceholder(locationP);

//#endregion

//#region animationloop
var movementEnabled = true;

//animation
renderer.setAnimationLoop(function () {
  if (renderer.xr.isPresenting && movementEnabled) {
    moveUser();
  }

  if (renderer.xr.isPresenting) {  
    positionBeenReset = false;
    setUserPosition();
  } 
  else {
    userPositionBeenSet = false;
    resetPosition();
  }
  directCrosshair();
  checkLookDirection();
  moveArrowMarker();
  renderer.render(scene, camera);
});

var positionBeenReset = false;
function resetPosition() {
  if (!positionBeenReset) {
    user.position.copy(userStartPosition);
    camera.position.copy(cameraStartPosition);
    positionBeenReset = true;
  }
}

var userPositionBeenSet = false;
function setUserPosition(){
  if(!userPositionBeenSet) {
    user.position.copy(userStartPosition);
    userPositionBeenSet = true;
  }
}

function moveUser() {
  var vector = new THREE.Vector3(0, 0, -0.03);
  var direction = vector.applyQuaternion(camera.quaternion);
  direction.y = 0; //no up or down
  user.position.add(direction);
}

function directCrosshair() {
  var vector = new THREE.Vector3(0, 0, -1);
  var direction = vector.applyQuaternion(camera.quaternion);
  direction.y += 1.62;
  crosshair.position.copy(direction);

  button.rotation.z = camera.rotation.z;
  buttonIndicator.rotation.z = camera.rotation.z;
}

const lookDownVector = new THREE.Vector3(0, -1, 0);

function checkLookDirection() {
  var lookAtVector = new THREE.Vector3(0, 0, -1);
  lookAtVector.applyQuaternion(camera.quaternion);

  if (lookAtVector.angleTo(lookDownVector) < Math.PI / 8) {
    fillIndicator();
  } else {
    watch.stop();
    watch.elapsedTime = 0;
    updateIndicator();
  }

  if (renderer.xr.isPresenting) {
    infoplaceholdersGroup.children.forEach((element, index) => {
      var directionToObject = new THREE.Vector3(
        element.position.x - user.position.x,
        element.position.y - camera.position.y,
        element.position.z - user.position.z
      );
      if (lookAtVector.angleTo(directionToObject) < (Math.PI / 180) * 5) {
        element.visible = false;
        infopanelsGroup.children[index].visible = true;
      } else {
        element.visible = true;
        infopanelsGroup.children[index].visible = false;
      }
    });
  }
}

var watch = new Clock();
function fillIndicator() {
  if (!watch.running) watch.start();
  else if (watch.getElapsedTime() >= buttonPressTime) {
    watch.stop();
    watch.elapsedTime = 0;
    movementEnabled = !movementEnabled;
    button.material.map = movementEnabled ? standStillImage : walkImage;
  }
  updateIndicator();
}

function updateIndicator() {
  buttonIndicator.geometry.dispose();
  var fillAngle = (watch.getElapsedTime() / buttonPressTime) * Math.PI * 2;
  buttonIndicator.geometry = new CircleGeometry(
    0.55,
    32,
    Math.PI / 2 - fillAngle,
    fillAngle
  );
}

function moveArrowMarker() {
  var angle = (-35 * Math.PI) / 180;
  var angle2 = (-55 * Math.PI) / 180;
  var x = camera.position.x / 85000;
  var z = camera.position.z / 85000;

  var long = 3.7090911 - Math.cos(angle) * x - Math.sin(angle2) * z;
  var lat = 51.0601373 - Math.sin(angle) * x + Math.cos(angle2) * z;

  arrowMarker.setGeometry(new Point(fromLonLat([long, lat])));
  map.getView().setCenter(transform([long, lat], "EPSG:4326", "EPSG:3857"));

  var euler = new THREE.Euler();
  euler.setFromQuaternion(camera.quaternion);
  var relativeAngle = euler.z;
  var mapAngle = relativeAngle + (132.5 * Math.PI) / 180;
  map.getView().setRotation(mapAngle);
}
//#endregion

function printToConsole(value) {
  document.body.addEventListener("click", () => {
    console.log("value : " + value + "\n");
  });
}

export { camera };
