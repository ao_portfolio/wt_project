var moveForward = false;
var moveBackward = false;
var moveRight = false;
var moveLeft = false;

document.addEventListener("keydown", (event) => {
  switch (event.code) {
    case "ArrowUp":
    case "KeyZ":
      moveForward = true;
      break;

    case "ArrowDown":
    case "KeyS":
      moveBackward = true;
      break;

    case "ArrowLeft":
    case "KeyQ":
      moveLeft = true;
      break;
  
    case "ArrowRight":
    case "KeyD":
      moveRight = true;
      break;
  }
});

document.addEventListener("keyup", (event) => {
  switch (event.code) {
    case "ArrowUp":
    case "KeyZ":
      moveForward = false;
      break;

    case "ArrowDown":
    case "KeyS":
      moveBackward = false;
      break;

    case "ArrowLeft":
    case "KeyQ":
      moveLeft = false;
      break;
  
    case "ArrowRight":
    case "KeyD":
      moveRight = false;
      break;
  }
});